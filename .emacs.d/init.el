;;; init.el --- jldt's Emacs configuration.
;;;
;;; Author: jldt
;;; URL   : https://gitlab.com/jldt/dotfiles
;;; Licence: GNUGPLv3
;;;
;;; Commentary:
;;;
;;; Code:

;;; -*- lexical-binding: t; -*-

;; I do not use `custom-set-*' so I'd rather have everything in a separate file
;; that I can load or not.
(setq-default custom-file (expand-file-name "custom.el" user-emacs-directory))

;;; IMPROVE (STARTUP) SPEED ------------------------------------------------------
;; References:
;; * https://www.reddit.com/r/emacs/comments/3kqt6e/2_easy_little_known_steps_to_speed_up_emacs_start/
;; * https://github.com/noctuid/dotfiles/blob/master/emacs/.emacs.d/init.el
;;
;; What are those lines doing?
;;
;; 1. When emacs is launched, set the garbage collection threshold to an
;; unreasonnable value, i.e. `most-positive-fixnum'. This is just to avoid
;; garbage collection during startup.
;;
;; 2. `run-with-idle-timer' will wait for 10 seconds of inactivity and set back
;; the garbage collection threshold to its standard value, displaying a message
;; at the same time.
(setq gc-cons-threshold most-positive-fixnum)

(run-with-idle-timer
 10 nil
 (lambda ()
   ;; 100mb
   (setq gc-cons-threshold 100000000)
   (message "gc-cons-threshold restored to %S" gc-cons-threshold)))


;; PACKAGES --------------------------------------------------------------------
(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org"   . "https://orgmode.org/elpa/")
                         ("gnu"   . "https://elpa.gnu.org/packages/")))
(package-initialize)


;; USE-PACKAGE -----------------------------------------------------------------
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)
(require 'use-package-ensure)
(setq use-package-always-ensure t)


;;; BETTER DEFAULTS ------------------------------------------------------------

;; Start maximized.
;;
;; If I don't put this before the part that removes the vertical scroll bar
;; then there is a glitch and the bar is back.
(setq default-frame-alist
      '((fullscreen . maximized)
        (fullscreen-restore . fullboth)))

;; Remove bars, scroll bars, etc…
(menu-bar-mode              -1)
(tool-bar-mode              -1)
(scroll-bar-mode            -1)
(horizontal-scroll-bar-mode -1)

;; Cursor should not blink: takes up CPU and it distracts me.
(blink-cursor-mode          -1)

;; A very annoying behavior of Emacs: it will round the size of the window if
;; it is not a multiple of the size of a character… Setting
;; `frame-resize-pixelwise' to true fixes this.
;;
;; (setq frame-resize-pixelwise t)
;;
;; At the same time it distorts the the font and it gave something pretty ugly
;; in the end.

(setq backup-directory-alist '(("." . "/tmp")))

(defalias 'yes-or-no-p #'y-or-n-p)

(show-paren-mode    1)
;; For more customization possibilities:
;; http://ergoemacs.org/emacs/emacs_insert_brackets_by_pair.html
(electric-pair-mode 1)

;; Scrolling.
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Auto-Scrolling.html#Auto-Scrolling
;; "If you set `scroll-conservatively' to a large number (larger than 100),
;; automatic scrolling never centers point, no matter how far point moves;
;; Emacs always scrolls text just enough to bring point into view, either at
;; the top or at the bottom of the window, depending on the scroll direction."
(setq scroll-conservatively most-positive-fixnum)

;; Prettify line number format and only show line numbers in programming modes.
(require 'linum)
(setq linum-format "%3d")
(require 'display-line-numbers)
(setq display-line-numbers-type 'relative)
(add-hook 'prog-mode-hook #'display-line-numbers-mode)

(column-number-mode  t)
(setq-default fill-column 79)
(add-hook 'text-mode-hook 'turn-on-visual-line-mode)

;; Trailing whitespaces, tabs and hard spaces display.
(setq nobreak-char-display t)
(require 'whitespace)
(setq whitespace-style '(face
                         trailing
                         tab-mark
                         space-mark))
(setq whitespace-display-mappings
      ;; KIND:
      ;; - `tab-mark'     : for all TAB characters;
      ;; - `space-mark'   : for all SPACE characters;
      ;; - `newline-mark' : for NEWLINE.
      ;;
      ;; CHAR: the character to be mapped.
      ;;
      ;; VECTOR: a series of characters to be displayed instead of the original
      ;; character.
      ;;
      ;; TIP: `describe-char'!
      ;;
      ;; ░ -> 9617
      ;;
      ;; ▒ -> 9618
      ;;
      ;; ▓ -> 9619
      ;;
      ;; KIND         CHAR     VECTORS
      '((space-mark   160      [9617] [95])             ; hard space
        (space-mark   32       [32] [183] [46])         ; spaces
        (tab-mark     ?\t      [9619 9619 9619 9619] [187 9] [92 9]))) ; tabs

(global-whitespace-mode 1)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq-default indent-tabs-mode nil
              tab-width        4)
(setq require-final-newline     t
      mouse-yank-at-point       t
      delete-old-versions       -1
      inhibit-startup-screen    t
      inhibit-x-resources       t
      ring-bell-function        'ignore
      sentence-end-double-space nil
      load-prefer-newer         t)

;; WARNING:
;; https://emacs.stackexchange.com/questions/13965/installing-auctex-from-package-manager-scan-error
;;
;; Setting the following lines (at least for read) overrides the setting for
;; individual files which led the package manager from being unable to compile
;; AUCTeX because of the japanese "alphabet".
;;
;; (setq coding-system-for-read    'utf-8
;;       coding-system-for-write   'utf-8)
;;
;; The correct way is as following:
;; http://ergoemacs.org/emacs/emacs_encoding_decoding_faq.html
(set-language-environment "UTF-8")


;; CUSTOM HELPER FUNCTIONS -----------------------------------------------------
(defun jldt/unfill-region (start end)
  "Unfill the region from START to END."
  (interactive "r")
  (let ((fill-column most-positive-fixnum))
    (fill-region start end)))


;;; EXEC-PATH-FROM-SHELL -------------------------------------------------------
(use-package exec-path-from-shell)

(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))


;;; ADAPTIVE-WRAP --------------------------------------------------------------
(use-package adaptive-wrap)


;;; INDENT-GUIDE ---------------------------------------------------------------
(use-package indent-guide
  :commands indent-guide-mode
  :hook (prog-mode . indent-guide-mode))


;;; WHICH-KEY ------------------------------------------------------------------
(use-package which-key
  :init
  (which-key-mode t))


;;; MODELINE -------------------------------------------------------------------
(use-package doom-modeline
  :hook (after-init . doom-modeline-mode)
  :init
  (setq doom-modeline-buffer-file-name-style 'buffer-name)
  ;; As per the modeline configuration: "While using the server mode in GUI,
  ;; should set the value explicitly."
  (setq doom-modeline-icon t)
  ;; (setq doom-modeline-bar-width 6)
  ;; (setq doom-modeline-height 10)
  (setq doom-modeline-modal-icon nil))


;;; EVIL + GENERAL -------------------------------------------------------------
(defun jldt/load-init-file ()
  "Load the init file."
  (interactive)
  (find-file user-init-file))

;; Available states are:
;; normal, insert, visual, replace, operator, motion.
(use-package evil
  :init
  (setq evil-want-C-u-scroll          t
        evil-search-wrap              nil
        evil-respect-visual-line-mode nil  ;; `nil' means disabled
        ;; The cursor is allowed to go one character after the end of the line
        ;; just like in Emacs.
        evil-move-beyond-eol          t)
  :config
  (evil-set-initial-state 'eshell-mode 'emacs)
  (evil-set-initial-state 'dired-mode 'emacs)
  (evil-set-initial-state 'wdired-mode 'normal)
  (evil-set-initial-state 'messages-buffer-mode 'emacs)
  (evil-set-initial-state 'help-mode 'emacs)
  (evil-set-initial-state 'special-mode 'emacs)
  (evil-mode t))

(use-package general)

;; Remove Evil default keybindings, since I'm not using Azerty/Qwerty but BÉPO.
(general-unbind '(normal visual motion operator replace)
  "h"
  "j"
  "k"
  "l"
  "H"
  "J"
  "K"
  "L"
  "t"
  "s"
  "r"
  "n"
  "T"
  "S"
  "R"
  "N"
  "M-t")

;; If I want to use `SPC' in motion, I first have to unbind it.
;; `general-unbind' does not seem to work (I'm probably calling it not as it
;; should).
(general-def
 :states 'motion
 "SPC"   nil)

(general-def '(normal motion emacs)
  :prefix            "SPC"
  :non-normal-prefix "C-M-SPC"

  ;; Buffers manipulation.
  "b"   '(:ignore t             :which-key "Buffer")
  "b b" '(ibuffer               :which-key "List buffers")
  "b p" '(switch-to-prev-buffer :which-key "Prev buffer")
  "b n" '(switch-to-next-buffer :which-key "Next buffer")
  ;; Shortcuts.
  "TAB"       '(switch-to-prev-buffer :which-key "Prev buffer")
  "<backtab>" '(switch-to-next-buffer :which-key "Next buffer")

  ;; Emacs interaction.
  "e"   '(:ignore t           :which-key "Emacs")
  "e p" '(list-packages       :which-key "List Packages")
  "e e" '(jldt/load-init-file :which-key "Edit init file")

  ;; Launch applications.
  "a"   '(:ignore t :which-key "Applications"))

(general-def 'visual
  :prefix "SPC"
  "a" '(align-regexp :which-key "Align regexp"))

(general-def '(normal visual motion)
  ;; Both "t" and "n" do NOT use the evil variation of the backward and forward
  ;; character functions *because* when there is an ellipsis at the end of a
  ;; line in an org file then the character is stuck… And this is (was)
  ;; annoying.
  "t" 'backward-char
  "n" 'forward-char
  "R" 'evil-window-top
  "k" 'evil-substitute
  "h" 'evil-replace
  "l" 'evil-search-next
  "L" 'evil-search-previous
  "j" 'evil-find-char-to
  "J" 'evil-find-char-to-backward
  "é" 'evil-forward-word-begin
  "É" 'evil-forward-WORD-begin
  "s" 'evil-next-visual-line
  "r" 'evil-previous-visual-line)

(general-def '(normal)
  "S" 'evil-window-bottom)

(general-define-key
  :keymaps '(evil-outer-text-objects-map evil-inner-text-objects-map)
  "é" 'evil-inner-word
  "É" 'evil-inner-WORD)

(use-package ace-window
  :init
  (setq aw-keys '(?a ?u ?i ?e ?t ?s ?r ?n ?m))
  (setq aw-dispatch-always t)
  (defvar aw-dispatch-alist
    '((?d aw-delete-window "Delete window")
      (?S aw-swap-window "Swap Windows")
      ;; (?m aw-move-window "Move Window")
      ;; (?C aw-copy-window "Copy Window")
      ;; (?j aw-switch-buffer-in-window "Select Buffer")
      ;; (?n aw-flip-window)
      ;; (?u aw-switch-buffer-other-window "Switch Buffer Other Window")
      ;; (?c aw-split-window-fair "Split Fair Window")
      (?h aw-split-window-vert "Split Window Vertically")
      (?v aw-split-window-horz "Split Window Horizontally")
      (?D delete-other-windows "Delete Other Windows"))
      ;; (?? aw-show-dispatch-help))
    "List of actions for `aw-dispatch-default'.")

  :general
  (general-def
    ;; alt gr + s
    "ß"  '(ace-window :which-key "Ace Window")))

(use-package avy
  :general
  (general-def
    "M-s" 'avy-goto-char-timer))

;; Using `S' to surrond the selected region with parenthesis, brackets, etc…
(use-package evil-surround
  :init
  (global-evil-surround-mode 1))

;; Using `gc' to (un)comment the selected lines and `gcc' for the line at the
;; cursor position.
(use-package evil-commentary
  :init
  (evil-commentary-mode))

;; `C-s' saves the current buffer.
(general-def
  [remap isearch-forward] 'save-buffer)

;; -----
;; Remaping some of the keys in common modes. Mainly I want "s" and "r" to
;; respectively go one line down and one line up.

(general-def
  :keymaps '(package-menu-mode-map
             help-mode-map
             ibuffer-mode-map)

  "s" 'next-line
  "r" 'previous-line)

;; PACKAGE MENU ---
(general-def
  :keymaps 'package-menu-mode-map

  "n" nil
  "p" nil

  "/" 'ctrlf-forward-fuzzy-regexp
  "?" 'ctrlf-backward-fuzzy-regexp
  "R" 'package-menu-refresh)

;; DIRED ---
(use-package dired-narrow
  :general
  (general-def
    :keymaps 'dired-mode-map

    "n" nil
    "p" nil

    "s" 'dired-next-line
    "r" 'dired-previous-line
    "/" 'dired-narrow))


;; HELP-MODE ---
(general-def
  :keymaps 'help-mode-map

  "S" 'help-go-forward
  "R" 'help-go-back)


;;; MAGIT ----------------------------------------------------------------------
(use-package magit
  :defer t
  :hook (magit-mode . (lambda () (setq truncate-lines nil)))
  :custom
  (magit-diff-refine-hunk 'all)
  :general
  (general-def '(normal emacs motion)
    :prefix "SPC"
    :non-normal-prefix "C-M-SPC"

    "m" '(:ignore t :which-key "Magit")

    "m s" '(magit-status :which-key "Status"))

  (general-def
    :keymaps 'magit-mode-map

    "r"   'magit-previous-line
    "s"   'magit-next-line
    "S"   'magit-stage
    "M-r" 'magit-rebase
    "M-s" 'magit-stage-file)

  (general-def
    :keymaps '(magit-staged-section-map
               magit-untracked-section-map
               magit-unstaged-section-map
               magit-hunk-section-map
               magit-file-section-map)

    "r"   'magit-previous-line
    "s"   'magit-next-line
    "S"   'magit-stage
    "M-s" 'magit-stage-file))


;;; DIFF-HL --------------------------------------------------------------------
(use-package diff-hl
  :config
  (setq diff-hl-draw-borders nil)
  :hook (after-init-hook . global-diff-hl-mode))


;;; IDO ------------------------------------------------------------------------
(general-def ido-completion-map
  "M-n" 'ido-next-match
  "M-t" 'ido-prev-match)


;;; UNDO-TREE ------------------------------------------------------------------
(use-package undo-tree
  :init
  (add-hook 'after-init-hook #'global-undo-tree-mode)
  (evil-set-initial-state 'undo-tree-visualizer-mode 'emacs)

  :general
  (general-def '(normal)
    "u"   'undo-tree-undo
    "C-r" 'undo-tree-redo
    "U"   'undo-tree-visualize))


;;; THEMING --------------------------------------------------------------------

;; Highlight line only in prog-mode.
(add-hook 'prog-mode-hook #'hl-line-mode)

;; See:
;; - https://protesilaos.com/codelog/2020-07-17-emacs-mixed-fonts-org/
;; - https://protesilaos.com/codelog/2020-09-05-emacs-note-mixed-font-heights/
(set-face-attribute 'default        nil :family "Iosevka" :weight 'light :height 180)
(set-face-attribute 'fixed-pitch    nil :family "Iosevka" :weight 'light)
(set-face-attribute 'variable-pitch nil :family "Fira Sans" :weight 'light)

;; Treat all the themes as safe.
(setq custom-safe-themes t)

;; (use-package doom-themes
;;   :custom
;;   (doom-themes-enable-bold   t)
;;   (doom-themes-enable-italic t)
;;   :config
;;   (doom-themes-org-config)
;;   (load-theme 'doom-spacegrey t))

;; Light variation, can be useful in a sunny environment.
(use-package modus-operandi-theme)

(use-package modus-vivendi-theme
  :custom
  (modus-operandi-theme-proportional-fonts t)
  (modus-operandi-theme-scale-headings t)
  :config
  (load-theme 'modus-vivendi t))

(use-package all-the-icons
  :custom
  ;; https://github.com/hlissner/doom-emacs/blob/develop/modules/ui/modeline/README.org#the-right-side-of-the-modeline-is-cut-off
  (all-the-icons-scale-factor 1))

;; https://github.com/Bassmann/emacs-config/blob/master/emacs.org
;; Specify a font for unicode characters.
(when (member "Symbola" (font-family-list))
  (set-fontset-font t 'unicode "Symbola" nil 'prepend))

;; When Prettify Symbols mode and font-locking are enabled, symbols are
;; prettified (i.e. displayed as composed characters) according to the rules in
;; `prettify-symbols-alist', which are locally defined by major modes
;; supporting prettifying.
(setq prettify-symbols-unprettify-at-point t)

;; `prettify-symbols-alist' automatically becomes buffer local when set. A hook
;; for the appropriate modes is necessary.
(defun jldt/prettify-symbols-prog ()
  "My custom list of symbols to prettify."
  (setq prettify-symbols-alist '(("<=" . ?≤)
                                 (">=" . ?≥)
                                 ("!=" . ?≠)))
  (prettify-symbols-mode +1))

(add-hook 'python-mode-hook #'jldt/prettify-symbols-prog)
(add-hook 'rust-mode-hook #'jldt/prettify-symbols-prog)


;;; NARROWING FRAMEWORK (IVY) --------------------------------------------------
;; As per the documentation, installing counsel installs everything.
(use-package counsel
  :custom
  (ivy-use-virtual-buffers t)
  (ivy-count-format "( %d/%d ) ")
  (ivy-wrap t)

  :general
  (general-def
    :states '(normal emacs)
    :prefix "SPC"
    :non-normal-prefix "C-M-SPC"

    "SPC" '(counsel-M-x       :which-key "M-x")
    "/"   '(swiper-isearch    :which-key "Swiper")
    "F"   '(counsel-find-file :which-key "Find file"))

  (general-def
    "M-x" 'counsel-M-x)

  :config
  (ivy-mode 1))

;; `Ivy-rich' a more friendly interface for Ivy.
(use-package ivy-rich
  :after (ivy)
  :config
  (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line)
  (ivy-rich-mode 1))

;; Makes everything more "intelligent", i.e. it remembers my choices.
(use-package ivy-prescient
  :after (counsel)
  :config
  (prescient-persist-mode)  ;; saves the history in a file.
  (ivy-prescient-mode t))


;;; CTRL-F ---------------------------------------------------------------------
;; I find that the `evil-search-*' functions are lacking a bit. `ctrlf-mode' is
;; a better alternative.
(use-package ctrlf
  :config
  (setq ctrlf-mode-bindings nil)
  (setq ctrlf-minibuffer-bindings '(("C-s" . ctrlf-forward-fuzzy-regexp)
                                    ("C-r" . ctrlf-backward-fuzzy-regexp)))

  :general
  (general-def
    :states 'normal

    "/" 'ctrlf-forward-fuzzy-regexp
    "?" 'ctrlf-backward-fuzzy-regexp))


;;; COMPANY --------------------------------------------------------------------
(use-package company
  :init
  (global-company-mode)
  :config
  (setq company-auto-complete-chars nil
        company-minimum-prefix-length 3
        company-idle-delay 0.3)
  (define-key company-active-map (kbd "M-n")   nil)
  (define-key company-active-map (kbd "M-p")   nil)
  (define-key company-active-map (kbd "C-n")   #'company-select-next)
  (define-key company-active-map (kbd "C-p")   #'company-select-previous)
  (define-key company-active-map (kbd "<tab>") #'company-complete-selection)
  (define-key company-active-map (kbd "C-w")   #'evil-delete-backward-word)
  ;; The following snippet was copied form here:
  ;; https://emacs.stackexchange.com/a/24800
  ;;
  ;; The idea is to accept a suggestion with `ENTER' only if there has
  ;; been an interaction with one of the proposed completions.
  ;;
  ;; "RET" is for terminal emacs, "<return>" is for windowed emacs.
  (dolist (key '("<return>" "RET"))
    (define-key company-active-map (kbd key)
      `(menu-item nil company-complete
                  :filter ,(lambda (cmd)
                             (when (company-explicit-action-p) cmd))))))

;; dabbrev-backend has a tendency to downcase everything, I don't like it.
(require 'company-dabbrev)
(setq company-dabbrev-downcase nil)


;;; FLYCHECK -------------------------------------------------------------------
(use-package flycheck
  :commands flycheck-list-errors flycheck-buffer
  :init (global-flycheck-mode)
  :hook (elpy-mode . flycheck-mode)
  :config
  ;; Check only when saving or opening files.
  (setq flycheck-check-syntax-automatically '(save mode-enabled))

  ;; Display errors a little quicker. Default is `0.9'.
  (setq flycheck-display-errors-delay 0.25)

  (general-def '(normal)
    :prefix "SPC"
    :non-normal-prefix "C-M-SPC"

    "f" '(:ignore t :which-key "Flycheck")
    "f n" '(flycheck-next-error :which-key "Next error")
    "f p" '(flycheck-previous-error :which-key "Previous error")))

(use-package flycheck-inline
  :after flycheck
  :hook (flycheck-mode . flycheck-inline-mode))


;;; YASNIPPET ------------------------------------------------------------------
(use-package yasnippet
  :general
  (general-define-key
   :keymaps 'yas-minor-mode-map

   "<tab>" nil
   "TAB"   nil
   "M-SPC" 'yas-expand))

(yas-global-mode 1)

(use-package yasnippet-snippets
  :after yas-minor-mode)


;; SPELL CHECKER ---------------------------------------------------------------
;; https://200ok.ch/posts/2020-08-22_setting_up_spell_checking_with_multiple_dictionaries.html
(with-eval-after-load "ispell"
  ;; Configure "LANG", otherwise `ispell' cannot find a default dictionary.
  (setenv "LANG" "en_US")
  (setq ispell-program-name "hunspell")
  ;; English and French dictionaries.
  (setq ispell-dictionary "en_US,fr_FR")
  ;; Apparently, `ispell-set-spellchecker-params' has to be called before
  ;; `ispell-hunspell-add-multi-dic'.
  (ispell-set-spellchecker-params)
  (ispell-hunspell-add-multi-dic "en_US,fr_FR")
  ;; Personal dictionary.
  (setq ispell-personal-dictionary "~/.hunspell_personal"))

;; The personal dictionary has to exist, otherwise hunspell will silently not
;; use it…
(unless (file-exists-p ispell-personal-dictionary)
  (write-region "" nil ispell-personal-dictionary nil 0))


;;; PROJECTILE -----------------------------------------------------------------
(use-package projectile
  :init (projectile-mode)
  :custom
  (projectile-sort-order        'recentf)
  (projectile-completion-system 'ivy)
  :config
  (general-def
   :states '(normal motion emacs)
   :prefix "SPC"
   :non-normal-prefix "C-M-SPC"

   "p"   '(projectile-command-map    :which-key "Projectile")))


;;; ELFEED ---------------------------------------------------------------------
(use-package elfeed
  :defer t

  :general
  (general-def
    :states '(normal emacs)
    :prefix "SPC"
    :non-normal-prefix "C-M-SPC"

    "a f" '(elfeed :which-key "Elfeed"))

  (general-def
    :keymaps 'elfeed-search-mode-map

    "n" nil
    "p" nil
    "r" 'previous-line
    "s" 'next-line
    "R" 'elfeed-search-untag-all-unread)

  (general-def
    :keymaps 'elfeed-show-mode-map

    "n"   nil
    "p"   nil
    "C-n" 'elfeed-show-next
    "C-p" 'elfeed-show-prev
    "r"   'previous-line
    "s"   'next-line)

  :config
  (evil-set-initial-state 'elfeed-search-mode 'emacs)
  (evil-set-initial-state 'elfeed-show-mode   'emacs))

(use-package elfeed-org
  :defer t
  :init
  (elfeed-org)
  (setq rmh-elfeed-org-files (list (concat user-emacs-directory "elfeed.org"))))


;;; ORG-MODE -------------------------------------------------------------------
(defun jldt/prettify-symbols-org ()
  "My custom list of symbols to prettify in `org-mode'."
  (setq prettify-symbols-alist '(("->" . ?→)
                                 ("=>" . ?⇒)
                                 ("=>" . ?⇒)
                                 ("!=" . ?≠)))
  (prettify-symbols-mode +1))

(use-package org
  :config
  ;; When this variable is set to `nil', Org will check if the date is
  ;; representable in the specific Emacs implementation you are using. If not,
  ;; it will force a year, usually the current year, and beep to remind you.
  (setq org-read-date-force-compatible-dates nil
        org-adapt-indentation                'headline-data
        org-src-fontify-natively             t
        org-fontify-whole-heading-line       t
        org-hide-emphasis-markers            nil
        org-hide-leading-stars               nil
        org-ellipsis                         "🢱")
  :hook ((org-mode . variable-pitch-mode)
         (org-mode . jldt/prettify-symbols-org)))

;; Pretty bullets.
(use-package org-superstar
  :after org
  :config
  (setq org-superstar-leading-bullet ?\s
        ;; "●" "◉" "◎" "○"
        org-superstar-headline-bullets-list '("●" "◉" "◎" "○")
        ;; "⬥" "⮞" "⬩" "↪"
        org-superstar-item-bullet-alist '((?+ . ?⮡)
                                          (?* . ?⬩)
                                          (?- . ?⬝)))
  :hook (org-mode . (lambda () (org-superstar-mode 1))))

;; Org-babel-load-languages
(org-babel-do-load-languages
 'org-babel-load-languages '((python . t)))

(setq org-image-actual-width nil)

;; CAPTURE ----
(setq org-directory "/home/julien/org/")
(setq org-default-notes-file (concat org-directory "/inbox.org"))

(require 'org-capture)
(setq org-capture-templates
      `(("t"                            ; key to select the template
         "Inbox entry"                  ; description
         entry                          ; type of entry
         (file org-default-notes-file)  ; where to store
         "*  TODO %?\nFiled: %U\n"     ; template
         )))

;; TASKS ---
;; Define the sequence of todo keywords.
;;
;; * The letter between paranthesis `(t)' indicates the shortcut that can be
;;   used to select the state when using `C-c C-t'.
;; * The `!' indicates that ONLY a timestamp should be recorded.
;; * The `@' indicates that a note AND a timestamp should be recorded.
;; * The `X/Y' means “do X when entering and Y when leaving the state”. No `/'
;;   means only do the action when entering.
(setq org-todo-keywords
      '((sequence " TODO(t)"
                  " IN-PROGRESS(p!)"
                  " WAITING(w@/!)"
                  "|"
                  " DONE(d!/@)"
                  " CANCELED(c@/@)")))

(setq org-todo-keyword-faces
      '((" CANCELED" . (:foreground "red" :weight bold))))

(setq org-enforce-todo-dependencies t)
(setq org-enforce-todo-checkbox-dependencies t)
(setq org-M-RET-may-split-line nil)

;; AGENDA --------------------------------------
(require 'org-agenda)
(use-package org-super-agenda
  :init
  (setq org-agenda-span 'week)
  (setq org-super-agenda-groups
         '((:name "MEETINGS"
                  :tag "meeting"
                  :face (:background "#457B9D")
                  :order 0)
           (:name "DEADLINES"
                  :and (:deadline t :not (:todo (" DONE" " CANCELED")))
                  :face (:background "#D62828")
                  :order 1)
           (:name "SCHEDULED"
                  :scheduled t
                  :order 2)))
  (evil-set-initial-state 'org-agenda-mode 'emacs)
  (evil-set-initial-state 'org-super-agenda-mode 'emacs)
  :hook (after-init . org-super-agenda-mode)
  :general
  (general-def
   :states '(emacs)
   :keymaps '(org-agenda-mode-map)

   "r"   'org-agenda-previous-line
   "s"   'org-agenda-next-line
   "M-n" 'org-agenda-later
   "M-r" 'org-agenda-redo
   "M-t" 'org-agenda-earlier
   "M-D" 'org-agenda-goto-today)

  (general-def
    :keymaps 'org-super-agenda-header-map

    "r" 'org-agenda-previous-line
    "s" 'org-agenda-next-line))

;; Where to look for agenda entries.
(setq org-agenda-files `(,(concat org-directory "agenda.org")
                         ,(concat org-directory "anniversaires.org")
                         ,(concat org-directory "tickler.org")
                         ,(concat org-directory "fêtes.org")
                         ,(concat org-directory "gtd.org")))

;; ORG-ROAM ------------------------------------
(use-package org-roam
  :hook (after-init . org-roam-mode)
  :custom (org-roam-directory "/home/julien/Zettelkasten/"))

;; Keymapings.
(general-def '(normal emacs motion)
 :prefix            "SPC"
 :non-normal-prefix "C-M-SPC"

 "o" '(:ignore t :which-key "Org")

 "o a" '(org-agenda-list :which-key "Agenda")

 "o c" '(org-capture :which-key "Capture")

 "o r" '(:ignore t :which-key "Org-Roam")
 "o r r" '(org-roam)
 "o r f" '(org-roam-find-file :which-key "Find file")
 "o r g" '(org-roam-show-graph :which-key "Show graph"))

(general-define-key
 :states '(normal visual insert emacs motion)
 :keymaps 'org-mode-map

  "M-t" 'org-metaleft
  "M-T" 'org-shiftmetaleft
  "M-n" 'org-metaright
  "M-N" 'org-shiftmetaright)
  ;; "S-tab" 'outline-hide-subtree)


;;; LANGUAGE SERVER PROTOCOL ---------------------------------------------------

;; Tweaks to improve the language server performance.
(setq read-process-output-max (* 1024 1024)) ;; 1 mb

(use-package lsp-mode
  :commands lsp lsp-install-server
  :hook ((lsp-mode . lsp-enable-which-key-integration))
  :init
  ;; Don't ask for the root of the project and try to guess it with the help of
  ;; `projectile'.
  (setq lsp-auto-guess-root t)
  ;; By default `lsp-mode' automatically activates `lsp-ui' unless
  ;; `lsp-auto-configure' is set to `nil'.
  (setq lsp-auto-configure nil)
  ;; Automatically kill the LSP server once the last buffer has been killed.
  (setq lsp-keep-workspace-alive nil)
  ;; How often `lsp-mode' will refresh the highlights, lenses, links, etc…
  ;; while typing.
  (setq lsp-idle-delay 0.500)
  ;; Include signature documentation in signature help.
  (setq lsp-signature-render-documentation nil)
  (setq lsp-server-install-dir "/home/julien/.config/LSP/"))

(use-package company-lsp
  :after (lsp-mode company)
  :init
  (push 'company-lsp company-backends)
  (setq company-lsp-cache-candidates 'auto
        company-lsp-enable-snippet t
        company-lsp-enable-recompletion t))

(use-package lsp-ui
  :after (lsp-mode)
  :hook (lsp-mode . lsp-ui-mode)
  :init
  (setq lsp-ui-doc-enable t
        lsp-ui-doc-position 'top
        lsp-ui-doc-delay 1.0)
  (setq lsp-ui-sideline-enable nil))


;;; PYTHON ---------------------------------------------------------------------
(use-package lsp-python-ms
  :init (setq lsp-python-ms-auto-install-server t)
  :hook (python-mode . (lambda ()
                         (require 'lsp-python-ms)
                         (lsp-deferred))))


;;; RUST -----------------------------------------------------------------------
(use-package rustic
  :init
  (setq rustic-lsp-server 'rust-analyzer
        rustic-lsp-client 'lsp-mode
        rustic-flycheck-clippy-params "--message-format=json")
  (add-to-list 'flycheck-checkers 'rustic-clippy)
  (add-to-list 'org-src-lang-modes '("rust" . rustic)))


;;; LATEX ----------------------------------------------------------------------
(use-package auctex
  :defer t)

(require 'tex)
(require 'reftex)
(setq reftex-toc-split-windows-horizontally t)
(setq reftex-toc-split-windows-fraction 0.2)
(setq reftex-auto-recenter-toc t)

;; To get support for the LaTeX packages, document parsing should be enabled.
(setq TeX-auto-save t)
(setq TeX-parse-self t)

;; Make AUCTeX aware of multi-file documents.
(setq-default TeX-master nil)

;; Enable folding.
(add-hook 'LaTeX-mode-hook 'outline-minor-mode)

;; References.
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)

(add-hook 'LaTeX-mode-hook (lambda ()
                             (push '(?\" . ("``". "''")) evil-surround-pairs-alist)))

;; Navigation in the Table of Contents minibuffer.
(general-def reftex-toc-mode-map
  "s" 'reftex-toc-next
  "r" 'reftex-toc-previous
  "R" 'reftex-toc-rescan)


;; FOCUSED-EDITING-MODE --------------------------------------------------------
;; Center the text in the frame for better focus.
(use-package olivetti
  :defer t
  :config
  (setq olivetti-body-width 0.6)
  (setq olivetti-minimum-body-width 80))

;;; init.el ends here
